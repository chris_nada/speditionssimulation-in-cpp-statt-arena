#include <iostream>
#include "src/etc.h"
#include "src/zeit.h"
#include "src/lager.h"
#include "src/transporter.h"

int main(int argc, char** argv) {

    const unsigned int tage_simulieren = 1'000;

    // Lager aufbauen
    std::vector<Lager*> lager;
    lager.push_back(new Lager("Warschau",  1, 1)); // 0
    lager.push_back(new Lager("Berlin",    2, 2)); // 1
    lager.push_back(new Lager("London",    3, 2)); // 2
    lager.push_back(new Lager("Glasgow",   4, 1)); // 3
    lager.push_back(new Lager("Barcelona", 1, 4)); // 4
    lager.push_back(new Lager("Madrid",    2, 3)); // 5
    lager.push_back(new Lager("Amsterdam", 3, 3)); // 6
    lager.push_back(new Lager("Rotterdam", 4, 4)); // 7

    // Innere Transporter aufbauen
    std::vector<Transporter> transporter;
    transporter.push_back(Transporter({lager[1], lager[2]}, Zeit::taeglich(), {8}, 18 * 60));
    //transporter.push_back(Transporter({lager[2], lager[1]}, Zeit::taeglich(), {8}, 18 * 60));
    transporter.push_back(Transporter({lager[1], lager[5]}, Zeit::taeglich(), {8}, 18 * 60));
    //transporter.push_back(Transporter({lager[5], lager[1]}, Zeit::taeglich(), {8}, 18 * 60));
    transporter.push_back(Transporter({lager[5], lager[6]}, Zeit::taeglich(), {8}, 18 * 60));
    //transporter.push_back(Transporter({lager[6], lager[5]}, Zeit::taeglich(), {8}, 18 * 60));
    transporter.push_back(Transporter({lager[2], lager[6]}, Zeit::taeglich(), {8}, 18 * 60));
    //transporter.push_back(Transporter({lager[6], lager[2]}, Zeit::taeglich(), {8}, 18 * 60));

    // Äußere Transporter aufbauen
    transporter.push_back(Transporter({lager[0], lager[1]}, {Zeit::MITTWOCH, Zeit::FREITAG}, {11, 14}, 4 * 60));
    transporter.push_back(Transporter({lager[2], lager[3]}, {Zeit::MITTWOCH, Zeit::FREITAG}, {11, 14}, 4 * 60));
    transporter.push_back(Transporter({lager[4], lager[5]}, {Zeit::MITTWOCH, Zeit::FREITAG}, {11, 14}, 4 * 60));
    transporter.push_back(Transporter({lager[6], lager[7]}, {Zeit::MITTWOCH, Zeit::FREITAG}, {11, 14}, 4 * 60));

    // Simulationsschleife
    int next_paket = 90;
    while(Zeit::get().get_tage_vergangen() <= tage_simulieren) {
        for (Transporter& lkw : transporter) lkw.simulieren();
        Zeit::weiter();

        next_paket += Zeit::SIMULATIONSSCHRITT;
        if (next_paket >= 90) {
            next_paket = 0;
            for (Lager* start_lager : lager) {
                Lager* ziel_lager = nullptr;
                do ziel_lager = lager[etc::zufall(0, lager.size() - 1)];
                while (start_lager == ziel_lager);
                start_lager->pakete_abgeben({new Paket(start_lager, ziel_lager)});
            }
        }
    }

    // Ergebnis ausgeben
    for (Transporter& lkw : transporter) lkw.print_statistik();

    // Speicher Aufräumen
    for (Lager* lager_ptr : lager) delete lager_ptr;
    std::cin >> argc;
    return 0;
}
