#include "zeit.h"

Zeit Zeit::globale_zeit;

void Zeit::print() const {
    std::vector<std::string> tage_strings {"Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"};
    std::cout << tage_strings[(int)tag] << ' ';
    std::cout << ((stunde >= 10) ? "" : "0") << stunde << ':';
    std::cout << ((minute >= 10) ? "" : "0") << minute << '\n';
}

void Zeit::weiter() {
    globale_zeit.minute += SIMULATIONSSCHRITT;
    while (globale_zeit.minute >= 60) {
        globale_zeit.minute -= 60;
        globale_zeit.stunde++;
    }
    while (globale_zeit.stunde >= 24) {
        globale_zeit.stunde -= 24;
        if ((int) globale_zeit.tag == SONNTAG) globale_zeit.tag = MONTAG;
        else globale_zeit.tag = (TAG) ((int) globale_zeit.tag + 1);
        globale_zeit.tage_vergangen++;
    }
}
