#pragma once

#include "lager.h"
#include "zeit.h"

/// Simulationsobjekt: Transporter.
class Transporter final {

public:

    /// ctor.
    Transporter(const std::vector<Lager*>& route,
                const std::vector<Zeit::TAG>& start_tag,
                const std::vector<uint16_t>& start_stunde,
                const uint16_t fahrtzeit) :
            route(route),
            start_tage(start_tag),
            start_stunden(start_stunde),
            FAHRTZEIT(fahrtzeit) {}

    /// Führt einen Simulationsschritt aus.
    void simulieren();

    /// Gibt Zwischeninfos aus.
    void inline log() {
        Zeit::get().print();
        std::cout << "LKW von " << route[0]->NAME << " nach " << route[1]->NAME << " faehrt los mit " << pakete.size() << " Paketen\n";
        statistik.push_back(pakete.size());
    }

    /// Gibt Statistik bis dato in die Konsole aus.
    void print_statistik() {
        long double x_auslastung = 0;
        for (auto x : statistik) x_auslastung += x;
        x_auslastung /= statistik.size();
        std::cout << "durchschnittliche Auslastung " << route[0]->NAME << '/' << route[1]->NAME << '=';
        std::cout << '\t' << x_auslastung << '\n';
    }

    /// Destruktor.
    virtual ~Transporter() { for (Paket* paket : pakete) delete paket; }

    /// Wieviele Minuten dauert eine Fahrt?
    const uint16_t FAHRTZEIT;

    /// True = Statistik nach Fahrt, False = Statistik nach Zeit.
    static const constexpr bool LOG_NACH_FAHRT = false;

private:

    /// Lädt passende Pakete vom gegebenem Lager.
    void laden(Lager* lager);

    /// Entlädt alle Pakete ins gegebene Lager.
    void entladen(Lager* lager);

    /// Restliche Minuten bis Ziel. Zählt runter.
    int minuten_bis_ziel;

    /// Geladene Pakete
    std::vector<Paket*> pakete;

    /// Führt Statistik über Pakete / Fahrt.
    std::vector<uint16_t> statistik;

    /// Route zwischen 2 Lagern.
    std::vector<Lager*> route;

    /// An welchen Tagen der Transporter losfährt.
    std::vector<Zeit::TAG> start_tage;

    /// Zu welchen Zeiten der Transporter losfährt.
    std::vector<uint16_t> start_stunden;

};