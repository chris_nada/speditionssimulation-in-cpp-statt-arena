#include "transporter.h"
#include "etc.h"
#include <algorithm>

void Transporter::simulieren() {
    if (!LOG_NACH_FAHRT) log();
    // Losfahren
    if ((std::find(start_tage.begin(), start_tage.end(), Zeit::get().get_tag()) != start_tage.end()) &&
            (Zeit::get().get_minute() == 0) &&
            (Zeit::get().get_stunde() == start_stunden[0])) {
        minuten_bis_ziel = FAHRTZEIT;
        laden(route[0]);
        if (LOG_NACH_FAHRT) log();
        return;
    }

    // Unterwegs
    minuten_bis_ziel -= Zeit::SIMULATIONSSCHRITT;

    // Angekommen
    if (minuten_bis_ziel <= 0) {
        entladen(route[1]);
        std::swap(route[0], route[1]);

        if (start_tage.size() == 2) {
            std::swap(start_tage[0],    start_tage[1]);
            std::swap(start_stunden[0], start_stunden[1]);
        }
    }
}

void Transporter::laden(Lager* lager) {
    std::vector<Paket*> zu_laden;
    for (Paket* paket : lager->get_pakete()) {
        // Wo muss das Paket hin?
        if (etc::distanz(paket->ZIEL->X, paket->ZIEL->Y, route[1]->X, route[1]->Y) <
            etc::distanz(paket->ZIEL->X, paket->ZIEL->Y, route[0]->X, route[0]->Y)) {
            zu_laden.push_back(paket);
            pakete.push_back(paket);
        }
    }
    lager->pakete_entnehmen(zu_laden);
}

void Transporter::entladen(Lager* lager) {
    lager->pakete_abgeben(pakete);
    pakete.clear();
}
