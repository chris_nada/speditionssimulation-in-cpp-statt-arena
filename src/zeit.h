#pragma once

#include <cstdint>
#include <iostream>
#include <array>
#include <vector>

/// Hält die Simulationszeit fest.
class Zeit final {

public:

    /// Aufzählung von Wochentagen.
    enum TAG {
        MONTAG = 0,
        DIENSTAG,
        MITTWOCH,
        DONNERSTAG,
        FREITAG,
        SAMSTAG,
        SONNTAG
    };

    /// Leerer ctor.
    Zeit() : minute(0), stunde(0), tag(MONTAG), tage_vergangen(0) {}

    /// Liefert die Zeit (global).
    static inline const Zeit& get() { return globale_zeit; }

    /// Setzt die Zeit einen Simulationsschritt weiter.
    static void weiter();

    /// Schreibt die aktuelle Zeit in eine Konsolenzeile.
    void print() const;

    /* Getter */
    inline TAG      get_tag()            const { return tag; }
    inline uint16_t get_minute()         const { return minute; }
    inline uint16_t get_stunde()         const { return stunde; }
    inline uint64_t get_tage_vergangen() const { return tage_vergangen; }

    /// Liefert einen `std::vector` mit einer geordneten Liste aller Wochentage; [0] = Montag.
    static inline const std::vector<TAG> taeglich() { return { MONTAG, DIENSTAG, MITTWOCH, DONNERSTAG, FREITAG, SAMSTAG, SONNTAG }; }

    /// Wieviele Minuten in einem Simulationsschritt vergehen.
    static constexpr const uint16_t SIMULATIONSSCHRITT = 30; // in Minuten

private:

    /// Quasi-Singleton für die Zeit.
    static Zeit globale_zeit;

    /* Speicher für die jetzige Zeit */
    TAG         tag;
    uint16_t    minute;
    uint16_t    stunde;
    uint64_t    tage_vergangen;

};