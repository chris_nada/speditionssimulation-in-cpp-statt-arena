#pragma once

// Vorwärtsdeklaration.
class Lager;

/// Simulationsobjekt Paket.
class Paket final {

public:

    /// ctor.
    Paket(const Lager* START, const Lager* ZIEL) : START(START), ZIEL(ZIEL) {}

    /// dtor.
    virtual ~Paket() = default;

    /// Startort.
    const Lager* START;

    /// Zielort.
    const Lager* ZIEL;

};