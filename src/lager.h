#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include "paket.h"

/// Simulationsobjekt: Lager.
class Lager final {

public:

    /// ctor.
    Lager(const std::string& NAME,
          const int X,
          const int Y) :
            NAME(NAME),
            X(X), Y(Y) {}

    /// Liefert die im Lager vorhandenen Pakete.
    const std::vector<Paket*>& get_pakete() const { return pakete; }

    /// Liefert gegebene Pakete ans Lager.
    void pakete_abgeben(const std::vector<Paket*>& temp_pakete) {
        for (Paket* paket : temp_pakete) {
            if (paket->ZIEL != this) pakete.push_back(paket);
            else delete paket; // Ziel erreicht!
        }
    }

    /// Entnimmt gegebene Pakete aus dem Lager.
    void pakete_entnehmen(const std::vector<Paket*>& temp_pakete) {
        for (Paket* paket : temp_pakete) {
            pakete.erase(std::find(pakete.begin(), pakete.end(), paket));
        }
    }

    /// Name (Ort) des Lagers.
    const std::string NAME;

    /// X-Koordinate.
    const int X;

    /// Y-Koordinate.
    const int Y;

private:

    /// Gelagerte Pakete.
    std::vector<Paket*> pakete;

};