#pragma once

#include <random>

namespace etc {

    static std::random_device rd;
    static std::mt19937 rng(rd());

    /// Liefert eine Zufallszahl zwischen `min` und `max` jeweils einschließlich.
    static int zufall(int min, int max) {
        std::uniform_int_distribution<int> uni(min, max);
        return uni(rng);
    }

    /// Gibt mit einer Wahrscheinlichkeit von `prozent` `true` zurück.
    static bool chance(int prozent) { return zufall(1, 100) >= prozent; }

    /// Liefert die Distanz zwischen zwei zweidimensionalen Punkten.
    static double distanz(int x1, int y1, int x2, int y2) {
        return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

}